import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {AngularFirestoreCollection} from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  private url = "https://6vq6kaqwbk.execute-api.us-east-1.amazonaws.com/beta";

  studentsCollection:AngularFirestoreCollection;

  predict(math:number, psycho:number, payed:boolean){
    let json = {
      "data":
    {
      "math": math,
      "psycho": psycho,
      "payed": payed
    }
    
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        let final:string = res.body;
        console.log(final);
        return final; 
      }))
      
    }
    

  addStudent(name:string, math: number, psycho: number, payed: boolean, result:string){
    const student = {name:name, math:math, psycho:psycho, payed:payed, result:result};
    console.log(student);
    this.db.collection(`students`).add(student)
    this.router.navigate(['/welcome']);
  }

  constructor(private db:AngularFirestore, public router:Router, private http:HttpClient) { }
}
