import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  customersCollection:AngularFirestoreCollection;

  getCustomers(userId){
    this.customersCollection = this.db.collection(`users/${userId}/customers`)
    return this.customersCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          console.log(data);
          return data;
        }
      )   ))
  }

  addCustomer(userId: string, name:string, education:number, income:number){
    const customer = {name:name , education:education , income:income};
    this.userCollection.doc(userId).collection('customers').add(customer);
  }
  deleteCustomer(userId:string , id:string){
    this.db.doc(`users/${userId}/customers/${id}`).delete();
  }

  updateCustomer(userId:string, id:string, name:string, education:number, income:number ){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        education:education,
        income:income

      }
    )
  }



  constructor( private db:AngularFirestore,
              private http:HttpClient) { }
}
