import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { Students } from '../interfaces/students';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {

  name:string;
  math:number;
  psycho:number;
  payed:boolean;
  arrPayed:Array<boolean> = [true, false];
  arrRes:Array<string> = ['Finish', 'Will not finish'];
  result:string;
  predictButton:boolean = true;



  closeForm(){
    this.predictButton = true;
    this.name = null;
    this.math = null;
    this.psycho = null;
    this.payed = null;

  }

  addStudent(){
    this.studentsService.addStudent(this.name, this.math, this.psycho, this.payed, this.result);
  }

  predict(){
    this.studentsService.predict(this.math, this.psycho, this.payed).subscribe(
      res => { 
        console.log(res);
        const resNum = Number(res);
        console.log(resNum);
        if(resNum > 0.5){
          var result = 'Finish';
        } else {
          var result = 'Will not finish'
        }
        this.result = result;
        this.predictButton = false;
        console.log(result);}
    );  
  }


  constructor(private studentsService:StudentsService) { }

  ngOnInit(): void {
  }

}
