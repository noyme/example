export interface Customer {
    id:string,
    name:string,
    education:number,
    income:number,
    predict?:string
}
