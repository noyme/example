export interface Students {
    name: string,
    math: number,
    psycho: number,
    payed: boolean,
    result?: string
}
